<!DOCTYPE html>
<html>
<?php
    require_once ('./PHP-MySQLi-Database-Class/MysqliDb.php');
    include 'dbconn.php';
    $db = OpenCon();

    // CREATE
    if (isset($_POST['create'])) {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];

        $data = Array ("first_name" => $first_name,
                        "last_name" => $last_name,
                        "birthday" => $birthday,
                        "address" => $address
        );

        $id = $db->insert ('employee', $data);
        if($id)
            echo 'New record was created.';

    }

    // if (isset($_POST['create'])) {
    //     $first_name = $_POST['first_name'];
    //     $last_name = $_POST['last_name'];
    //     $birthday = $_POST['birthday'];
    //     $address = $_POST['address'];

    //     $insertSql = "INSERT INTO employee (first_name, last_name, birthday, address) 
    //                 VALUES ('$first_name', '$last_name', '$birthday', '$address')";
    //     if ($conn->query($insertSql) === TRUE) {
    //         echo "Record created successfully.<br>";
    //     } else {
    //         echo "Error creating record: " . $conn->error . "<br>";
    //     }
    // }

    // UPDATE
    if (isset($_POST['update'])) {

        $id = $_POST['update_id'];
        $new_first_name = $_POST['new_first_name'];
        $new_last_name = $_POST['new_last_name'];
        $new_birthday = $_POST['new_birthday'];
        $new_address = $_POST['new_address'];

        $data = Array (
            'first_name' => $new_first_name,
            'last_name' => $new_last_name,
            'birthday' => $new_birthday,
            'address' => $new_address
        );
        $db->where ('id', $id);
        if ($db->update ('employee', $data))
            echo $db->count . ' records were updated';
        else
            echo 'update failed: ' . $db->getLastError();
    }

    // DELETE
    if (isset($_POST['delete'])) {
        $id = $_POST['delete_id'];

        $db->where('id', $id);
        if($db->delete('employee')) echo 'successfully deleted';
    }
?>
<head>
    <title>Employee CRUD</title>
</head>
<body>
    <h2>Create Employee</h2>
    <form method="POST">
        First Name: <input type="text" name="first_name"><br>
        Last Name: <input type="text" name="last_name"><br>
        Birthday: <input type="date" name="birthday"><br>
        Address: <input type="text" name="address"><br>
        <button type="submit" name="create">Create</button>
    </form>

    <h2>Employees</h2>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Birthday</th>
            <th>Address</th>
            <th>Actions</th>
        </tr>
        <?php
        // READ
        $users = $db->get('employee');

        if ($db->count > 0) {
            foreach ($users as $user) { 
                echo "<tr>";
                echo "<td>" . $user["id"] . "</td>";
                echo "<td>";
                echo $user["first_name"];
                echo "</td>";
                echo "<td>" . $user["last_name"] . "</td>";
                echo "<td>" . $user["birthday"] . "</td>";
                echo "<td>" . $user["address"] . "</td>";
                echo "<td>";
                if (isset($_POST['cancel'])) {
                    unset($_POST['edit_id']);
                }
                if (isset($_POST['edit']) && $_POST['edit_id'] == $user["id"]) {
                    echo "<form method='POST'>";
                    echo "<input type='hidden' name='update_id' value='" . $user["id"] . "'>";
                    echo "First Name: <input type='text' name='new_first_name' value='" . $user["first_name"] . "'><br>";
                    echo "Last Name: <input type='text' name='new_last_name' value='" . $user["last_name"] . "'><br>";
                    echo "Birthday: <input type='date' name='new_birthday' value='" . $user["birthday"] . "'><br>";
                    echo "Address: <input type='text' name='new_address' value='" . $user["address"] . "'><br>";
                    echo "<button type='submit' name='update'>Update</button>";
                    echo "</form>";
                    echo "<form method='POST'><button type='submit' name='cancel'>Cancel</button></form>";
                } else {
                    echo "<form method='POST'>";
                    echo "<input type='hidden' name='edit_id' value='" . $user["id"] . "'>";
                    echo "<button type='submit' name='edit'>Edit</button>";
                    echo "</form>";
                    echo "<form method='POST'>";
                    echo "<input type='hidden' name='delete_id' value='" . $user["id"] . "'>";
                    echo "<button type='submit' name='delete'>Delete</button>";
                    echo "</form>";
                }
                
                echo "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr><td colspan='6'>No results.</td></tr>";
        }
        ?>
    </table>
</body>
</html>
