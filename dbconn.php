<?php 
    // Connect to the database using MySQLi
    require_once ('./PHP-MySQLi-Database-Class/MysqliDb.php');
    
    function OpenCon() {
        $host = 'localhost';
        $username = 'root';
        $password = '';
        $database = 'employee_db';


        $db = new MysqliDb ($host, $username, $password, $database);

        if (!$db->ping()) {

            die("Connection failed: " . $db->error());

        } else {

            echo "Connection established successfully!<br>";
            return $db;
            
        }
    }


    function CloseCon($db)
    {
        $db->disconnect();
    }
?>